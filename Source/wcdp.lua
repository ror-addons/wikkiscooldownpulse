--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if not WCDP then WCDP = {} end

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local alpha = false
--[===[@alpha@
alpha = true
--@end-alpha@]===]

local T					= LibStub( "WAR-AceLocale-3.0" ) : GetLocale( "wcdp", debug )
local LSA				= LibStub( "LibSharedAssets" )

WCDP.db					= nil
local db				= nil	

local VERSION 			= { MAJOR = 2, MINOR = 0, REV = 4 }
local DisplayVersion 	= string.format( "%d.%d.%d", VERSION.MAJOR, VERSION.MINOR, VERSION.REV )

if( debug ) then
	DisplayVersion 			= DisplayVersion .. " Dev Build"
else
	DisplayVersion 			= DisplayVersion .. " r" .. ( tonumber( "23" ) or 0 )
	
	if( alpha ) then
		DisplayVersion = DisplayVersion .. "-alpha"
	end
end

local firstLoad 				= true
local blackList					= {}
local whiteList					= {}

local upgradeProfile

WCDP.DefaultSettings =
{
	general =
	{
		debug					= false,
		version					= VERSION,
		testmode				= false,
		enablewhitelist			= false,
		enableblacklist			= false,
		sound					= true,
	},
	
	anchor = 
	{
		point						= "center",
		relwin						= "Root",
		relpoint					= "center",
		scale						= 0.75,
		x							= 0,
		y							= 0,
		dx							= 192,
		dy							= 192,					
	},
	
	alpha = 
	{
		start						= .2,
		finish						= .6,
		speed						= .2,
	},
	
	blacklist						= {},
	whitelist						= {},
	
	abilities =
	{
		actions	= {
			enabled					= true,
			mincooldown				= 4,
		},
		morale	= {
			enabled					= true,
			mincooldown				= 4,
		},
		pet	= {
			enabled					= true,
			mincooldown				= 4,
		},
	},
}

local _lastMoraleAbilityCast		= -1
local _lastAbilityPulsed      		= 0
local runtimeWindow 				= "WCDPFrame"
local layoutWindow 					= "WCDPLayoutFrame"
local cooldownLookup				= {}
local GENERAL_GCD					= 1.61

function WCDP.GetVersion() return DisplayVersion end

function WCDP.OnInitialize()
	RegisterEventHandler( SystemData.Events.RELOAD_INTERFACE, "WCDP.OnLoad" )
	RegisterEventHandler( SystemData.Events.LOADING_END, "WCDP.OnLoad" )
end

function WCDP.OnLoad()
	if( firstLoad ) then
		-- Create our configuration objects
		WCDP.InitProfiles()
		
		local profileIdx, _ = WCDP.GetCurrentProfile()
		
		-- Update our local db with configuration information
		WCDP.db = WCDP.GetProfileDataForUse( profileIdx )
		db = WCDP.db
		
		-- Upgrade our profile
		upgradeProfile()
		
		-- Register our slash commands with LibSlash
		WCDP.Debug( "Registering slash commands" )
		
		-- Load our configuration window
		WCDPConfig.OnLoad()
	
		-- Update the layout window
		WCDP.UpdateLayoutWindow()
		
	    -- Update the runtime window
		WCDP.UpdateRuntimeWindow()
		
		-- Set its alpha to 0
		WindowSetAlpha( runtimeWindow, 0 )
		
		-- Show the window
		WindowSetShowing( runtimeWindow, true )
		
		-- Register to know when the layout editor is complete
	    table.insert( LayoutEditor.EventHandlers, WCDP.OnLayoutEditorFinished )
		
		-- Register an event for reload ui
		RegisterEventHandler( SystemData.Events.CUSTOM_UI_SCALE_CHANGED, "WCDP.OnUIScaleChange" )
		RegisterEventHandler( SystemData.Events.PLAYER_BEGIN_CAST, "WCDP.OnPlayerBeginCast" )
		
		--
		-- In order for us to get ability cooldowns, we need to snag this function for the data
		--
		WCDP.__ActionButton_UpdateCooldownAnimation					= ActionButton.UpdateCooldownAnimation
		ActionButton.UpdateCooldownAnimation 						= WCDP.OnAbilityUpdate
		
		--
		-- In order for us to get morale level timers, we need to snag this function for the data
		--
		WCDP.__MoraleButton_Update									= MoraleButton.Update
		MoraleButton.Update					 						= WCDP.OnMoraleUpdate
		
		-- Print our initialization usage
		local init = WStringToString( T["Wikki's Cooldown Pulse %s initialized."] )
		WCDP.Print( init:format( DisplayVersion ) )
		
		-- Attempt to register one of our handlers
		if( LibSlash ~= nil and type( LibSlash.RegisterSlashCmd ) == "function" ) then
			LibSlash.RegisterSlashCmd( "wcdp", WCDP.Slash )
			LibSlash.RegisterSlashCmd( "wcdpconfig", WCDP.Slash )
			LibSlash.RegisterSlashCmd( "showwcdp", WCDP.Slash )
			WCDP.Print( T["(Wikki's Cooldown Pulse) use /wcdp to bring up the configuration window."] )
		end
		
		d( "Wikki's Cooldown Pulse loaded." )
	
		firstLoad = false
	end
	
	-- Every time we zone reinitialize
	WCDP.Reinitialize()
end

function WCDP.Reinitialize()
	-- Rebuild our blacklist
	blackList = {}
	if( db.general.enableblacklist ) then
		for idx, ability in ipairs( db.blacklist )
		do
			blackList[ability] = true
		end
	end
	
	-- Rebuild our whitelist
	whiteList = {}
	if( db.general.enablewhitelist ) then
		for idx, ability in ipairs( db.whitelist )
		do
			whiteList[ability] = true
		end
	end
	
	cooldownLookup = {}
	cooldownLookup[Player.AbilityType.ABILITY] 	= db.abilities.actions.mincooldown
	cooldownLookup[Player.AbilityType.MORALE] 	= db.abilities.morale.mincooldown
	cooldownLookup[Player.AbilityType.PET] 		= db.abilities.pet.mincooldown
end

function WCDP.OnProfileChanged()
	local profileIdx, currentProfileName = WCDP.GetCurrentProfile()
	
	-- Update our local db with configuration information
	WCDP.db = WCDP.GetProfileDataForUse( profileIdx )
	db = WCDP.db
	
	-- Upgrade our profile
	upgradeProfile()
	
	-- Update the layout window
	WCDP.UpdateLayoutWindow()
	WCDP.UpdateRuntimeWindow()
	
	WCDP.Reinitialize()
	
	WCDP.Print( L"(WCDP) " .. T["Active Profile:"] .. L" '" .. currentProfileName .. L"'" )
end

function WCDP.OnLayoutEditorFinished( editorCode )
	if( editorCode == 2 ) then
		db.anchor.point, db.anchor.relpoint, db.anchor.relpoint, db.anchor.x, db.anchor.y 	= WindowGetAnchor( layoutWindow, 1 )
		db.anchor.dx, db.anchor.dy				= WindowGetDimensions( layoutWindow )
		db.anchor.scale 						= WindowGetScale( layoutWindow )
		
		WCDP.UpdateRuntimeWindow()
	end
end

function WCDP.UpdateLayoutWindow()
	LayoutEditor.UnregisterWindow( layoutWindow )
		
	-- Clear any anchors the window has
	WindowClearAnchors( layoutWindow )

	-- Set the anchor for its position on the screen
	WindowAddAnchor( layoutWindow,
		db.anchor.point,
		db.anchor.relwin,
		db.anchor.relpoint,
		db.anchor.x,
		db.anchor.y )
	
	-- Set the window's scale
	WindowSetScale( layoutWindow, db.anchor.scale )
	
	-- Set the dimensions
	WindowSetDimensions( layoutWindow, db.anchor.dx, db.anchor.dy )
	
	-- Register with the layout editor
 	LayoutEditor.RegisterWindow( layoutWindow, L"WCDP Anchor", L"Wikki's Cooldown Pulse Anchor", true, true, false, nil )
end

function WCDP.UpdateRuntimeWindow()
	-- Clear any anchors the window has
	WindowClearAnchors( runtimeWindow )

	-- Set the anchor for its position on the screen
	WindowAddAnchor( runtimeWindow, db.anchor.point, db.anchor.relwin, db.anchor.relpoint, db.anchor.x / InterfaceCore.GetScale(), db.anchor.y / InterfaceCore.GetScale() )
	
	-- Set the window's scale
	WindowSetScale( runtimeWindow, db.anchor.scale )
	
	-- Set the dimensions
	WindowSetDimensions( runtimeWindow, db.anchor.dx, db.anchor.dy )
end

function WCDP.OnUIScaleChange()
	-- Update the layout wndow
	WCDP.UpdateLayoutWindow()
	-- Update the runtime window
	WCDP.UpdateRuntimeWindow()
	-- Save the layout window position
	WCDP.OnLayoutEditorFinished( 2 )
end

function WCDP.OnPlayerBeginCast( abilityId, isChannel, desiredCastTime, averageLatency )
	if( desiredCastTime == 0 ) then
		local abilityData = Player.GetAbilityData( abilityId )
		if( abilityData ~= nil and abilityData.abilityType == Player.AbilityType.MORALE ) then
			WCDP.Debug( "Storing morale ability id: " .. tostring( abilityId ) )
			_lastMoraleAbilityCast = abilityId
		end
	end
end

function WCDP.OnAbilityUpdate( ... )
	local self = ...
	local initialMaxCooldown 	= self.m_MaxCooldown
	
	-- Call the original function
	WCDP.__ActionButton_UpdateCooldownAnimation( ... )
	
	-- Perform the ability update check
	WCDP.PerformAbilityUpdate( self.m_ActionId, self.m_Cooldown, initialMaxCooldown, self.m_MaxCooldown )
end

function WCDP.OnMoraleUpdate( ... )
	local self = ...
	local initialMaxCooldown = self.m_MaxCooldown
	
	-- Call the original function
	WCDP.__MoraleButton_Update( ... )
	
	--
	-- All morale abilities come off of cooldown at the same time, check to see if this was the morale ability fired
	--
	if( _lastMoraleAbilityCast ~= self.m_AbilityId ) then return end
	
	-- Perform the ability update check
	WCDP.PerformAbilityUpdate( self.m_AbilityId, self.m_Cooldown, initialMaxCooldown, self.m_MaxCooldown )
end

function WCDP.PerformAbilityUpdate( abilityId, cooldown, initialMaxCooldown, currentMaxCooldown )
	if( abilityId == nil or cooldown == nil or initialMaxCooldown == nil or currentMaxCooldown == nil ) then return end
	
	--
	-- If the ability is in our black list, we are not pulsing it
	--
	if( blackList[abilityId] ~= nil ) then return end
	
	local abilityData = Player.GetAbilityData( abilityId )
	
	-- If we cant pull ability data on the ability, black list it so we do not try any further
	if( abilityData == nil or ( abilityData ~= nil and abilityData.abilityType == nil ) ) then
		blackList[abilityId] = true
		return
	end
	
	local abilityType = abilityData.abilityType
	
	if cooldownLookup[abilityType] == nil then return end
	
	-- If the ability isnt white listed check to see if we can white/black list it
	if( whiteList[abilityId] == nil ) then
		if( db.general.enablewhitelist and not whiteList[abilityId] ) then
			return
		else
			if( ( abilityType == Player.AbilityType.ABILITY and not db.abilities.actions.enabled ) or 
				( abilityType == Player.AbilityType.MORALE and not db.abilities.morale.enabled ) or
				( abilityType == Player.AbilityType.PET and not db.abilities.pet.enabled ) ) then
				-- We do not need a pulse for this ability so black list it
				blackList[abilityId] = true
				return
			end
			
			-- If we got here, whitelist the ability
			whiteList[abilityId] = true
		end
	end
	
	if( initialMaxCooldown > GENERAL_GCD and ( initialMaxCooldown >= cooldownLookup[abilityType] ) and cooldown <= 0  ) then
		local displayTexture, x, y, disabledTexture
		
		WCDP.Debug( "AbilityId:  " .. tostring( abilityId ) .. " Cooldown:  " .. tostring( cooldown ) .. "  Max: " .. tostring( initialMaxCooldown ) )
		
		-- Check to see if we pulsed this ability less than a GCD ago so we do not pulse this again
		-- This fixes a client issue where morale updates are sent with 0 cooldown, 60 max until another
		-- ability is used.  Hopefully 1.0.5 fixes this.
		if( _lastAbilityPulsed == _lastMoraleAbilityCast and cooldown < GENERAL_GCD ) then
			return 
		end
		
		--
		-- Check to see if we have the texture cached
		if( abilityData ~= nil ) then
			-- Retrieve the ability texture information
			displayTexture, x, y, disabledTexture = GetIconData( abilityData.iconNum )
			
			if( displayTexture ~= nil ) then
				DynamicImageSetTexture( runtimeWindow .. "ImageSquare", displayTexture, 0, 0 )
				
				WindowStartAlphaAnimation( runtimeWindow, Window.AnimationType.LOOP, db.alpha.start, db.alpha.finish, db.alpha.speed, true, 0, 1 )
				
				if( db.general.sound ) then
					Sound.Play( GameData.Sound.WINDOW_CLOSE )
				end
				
				_lastAbilityPulsed = abilityId
			end
		end
	end
end

function WCDP.Slash( input )
	WCDP.ToggleConfig()
end

function WCDP.ToggleConfig()
	if( DoesWindowExist( WCDPConfig.GetWindowName() ) ) then
		WindowSetShowing( WCDPConfig.GetWindowName(), not WindowGetShowing( WCDPConfig.GetWindowName() ) )
	end
end

function WCDP.Print( str )
	EA_ChatWindow.Print( towstring( tostring( str ) ), ChatSettings.Channels[SystemData.ChatLogFilters.SAY].id );
end

function WCDP.Debug(str)
	if( db.general.debug ) then
	 	DEBUG( towstring( str ) )
	end
end

function upgradeProfile()
	local oldVersion = db.general.version
	db.general.version = VERSION
	
	-- add any processing here on version change
end