--
-- This file adapted from SquaredProfiles.lua, from Aiiane's Squared, to work with this addon
--
local WCDP = WCDP

-- If it doesn't already exist, make it
if not WCDP.Profiles then WCDP.Profiles = {active=0,names={},data={},perchar={}} end

-- Make utility functions local for performance
local pairs 			= pairs
local unpack 			= unpack
local tonumber			= tonumber
local tostring 			= tostring
local towstring 		= towstring
local max 				= math.max
local min 				= math.min
local wstring_sub 		= wstring.sub
local wstring_format 	= wstring.format
local tinsert 			= table.insert
local tremove 			= table.remove

-- generic deepcopy
local function deepcopy(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end
        return new_table
    end
    return _copy(object)
end

-- This function takes a defaults table and a settings, and verifies that all 
-- keys that exist in the defaults exist in the settings.
local function updateSettings(defaults, settings)
	if type(defaults) ~= "table" or type(settings) ~= "table" then return end
	for key, value in pairs( defaults ) do
		if type(value) ~= "table" and value ~= nil then
			-- Check if the key exists in settings
			if settings[key] == nil then
				settings[key] = value
			end
		else
			-- Create the table if it doesnt exist, or if its type is not of table
			if settings[key] == nil or type(settings[key]) ~= "table" then
				settings[key] = {}
			end
			
			updateSettings( value, settings[key] )
		end
	end	
end

local function GetCurrentSlot()
	for k,v in pairs(GameData.Account.CharacterSlot) do
		if v.Name == GameData.Player.name then
			return k
		end
	end
	return nil
end

function WCDP.Set(key, value)
	if key then
		WCDP.Profiles.data[WCDP.Profiles.active][key] = value
	end
end

function WCDP.Get( key )
	if key then
		return WCDP.Profiles.data[WCDP.Profiles.active][key]
	end
	return nil
end

function WCDP.ResetActiveProfile()
	if WCDP.Profiles and WCDP.Profiles.data and WCDP.Profiles.data[WCDP.Profiles.active] then
		WCDP.Profiles.data[WCDP.Profiles.active] = deepcopy(WCDP.DefaultSettings)
		WCDP.OnProfileChanged()
	end
end

-- Initialize the profile system (called from WCDP.OnLoad)
function WCDP.InitProfiles()
    if WCDP.Profiles.active == 0 then
		-- Empty profiles table - initialize the 'Default' profile
		WCDP.Profiles.active = WCDP.AddProfile(L"Default")
		local slot = GetCurrentSlot()
		if not slot then d("Couldn't figure out what character slot this is!") return end
		WCDP.Profiles.perchar[slot] = L"Default"
	else
		-- Non-empty profiles table - check for smart activation
		local slot = GetCurrentSlot()
		local profile = WCDP.Profiles.perchar[slot]
		if profile then
			WCDP.ActivateProfile(profile)
		else 
			WCDP.Profiles.perchar[slot] = L"Default"
			WCDP.ActivateProfile(L"Default")
		end
	end
end

function WCDP.AddProfile(name, source)
	local sourceId = nil
	
	-- First check to see if we're adding a profile which already exists,
	-- if we are then just return that profile's id
	for k,v in ipairs(WCDP.Profiles.names) do
		if v == name then return k end
		-- Might as well find the source Id while we're at it
		if v == source then sourceId = k end
	end
	
	-- If it doesn't exist, add it
	tinsert(WCDP.Profiles.names, name)
	local data = WCDP.GetProfileData(sourceId)
	if data then
		tinsert(WCDP.Profiles.data, data)
	else
		tinsert(WCDP.Profiles.data, deepcopy(WCDP.DefaultSettings))
	end
	
	return #WCDP.Profiles.names
end

function WCDP.GetProfileData(source)
	-- If specified by name, look up the id
	if type(source) == "wstring" then
		for k,v in ipairs(WCDP.Profiles.names) do
			if v == source then
				source = k
				break
			end
		end
	end
	
	if type(source) == "number" then
		return deepcopy(WCDP.Profiles.data[source])
	else
		-- invalid profile
		return nil
	end
end

function WCDP.GetProfileDataForUse(source)
	-- If specified by name, look up the id
	if type(source) == "wstring" then
		for k,v in ipairs(WCDP.Profiles.names) do
			if v == source then
				source = k
				break
			end
		end
	end
	
	if type(source) == "number" then
		return WCDP.Profiles.data[source]
	else
		-- invalid profile
		return nil
	end
end

function WCDP.ActivateProfile(source, force)
	-- Find the profile if specified by name
	if type(source) == "wstring" then
		for k,v in ipairs(WCDP.Profiles.names) do
			if v == source then source = k end
		end
	end
	
	if type(source) ~= "number" then return end
	local sourceName = WCDP.Profiles.names[source]
	if not force and WCDP.Profiles.active == source then return end
	
	local data = WCDP.GetProfileData(source)
	if not data then return end
	WCDP.Profiles.active = source
	
	-- Make sure the profile is up to date
	updateSettings( WCDP.DefaultSettings, WCDP.Profiles.data[WCDP.Profiles.active] )
	
	WCDP.OnProfileChanged()
	
	local slot = GetCurrentSlot()
	if slot then
		WCDP.Profiles.perchar[slot] = sourceName
	end

	return source
end

function WCDP.CopyProfileToActive( source )
	-- Find the profile if specified by name
	if type(source) == "wstring" then
		for k,v in ipairs(WCDP.Profiles.names) do
			if v == source then source = k end
		end
	end
	if type(source) ~= "number" then return end
	
	if WCDP.Profiles and WCDP.Profiles.data and WCDP.Profiles.data[source] and WCDP.Profiles.data[WCDP.Profiles.active] then
		WCDP.Profiles.data[WCDP.Profiles.active] = deepcopy(WCDP.Profiles.data[source])
		
		updateSettings( WCDP.DefaultSettings, WCDP.Profiles.data[WCDP.Profiles.active] )
		
		WCDP.OnProfileChanged()
	end
end

function WCDP.GetProfileList()
	return deepcopy(WCDP.Profiles.names)
end

function WCDP.GetCurrentProfile()
	return WCDP.Profiles.active, WCDP.Profiles.names[WCDP.Profiles.active]
end

function WCDP.DeleteProfile(source)
	-- Find the profile if specified by name
	if type(source) == "wstring" then
		for k,v in ipairs(WCDP.Profiles.names) do
			if v == source then source = k end
		end
	end
	
	if type(source) ~= "number" then return end
	
	-- Don't allow people to delete the default profile, or a nonexistant one
	if source < 2 or not WCDP.Profiles.names[source] then return end
	
	local sourceName = WCDP.Profiles.names[source]
	
	-- Remove the profile
	tremove(WCDP.Profiles.names, source)
	tremove(WCDP.Profiles.data, source)
	
	-- Swap any perchars which were the deleted profile back to default
	local slot = GetCurrentSlot()
	for k,v in pairs(WCDP.Profiles.perchar) do
		if v == sourceName then
			WCDP.Profiles.perchar[k] = L"Default"
			-- If our current character was set to the deleted profile, activate default
			if k == slot then
				WCDP.ActivateProfile(L"Default")
			end
		end
	end
end