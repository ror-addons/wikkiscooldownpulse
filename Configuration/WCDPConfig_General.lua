--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if not WCDPConfig then return end

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local T = LibStub( "WAR-AceLocale-3.0" ) : GetLocale( "wcdp", debug )
local LibGUI 	= LibStub( "LibGUI" )

local db

local initialized		= false

local InitializeWindow, ApplyConfigSettings, RevertConfigSettings

local startAlpha, endAlpha, speed

local window	= {
	Name		= T["General"],
	display		= {},
	W			= {},
}

function InitializeWindow()
	-- If our window already exists, we are all set
	if initialized then return end
	
	local w
	local e	
	
	-- Set our local database
	db = WCDP.db
	
	-- General Window
	w = LibGUI( "window", nil, "WCDPWindowDefault" )
	w:ClearAnchors()
	w:Resize( 400, 175 )
	w:Show( true )
	window.W.General = w
	
	-- Action Window
	w = LibGUI( "window", nil, "WCDPWindowDefault" )
    w:Resize( 400, 110 )
	w:Show( true )
	window.W.Actions = w
	
	-- Morale Window
	w = LibGUI( "window", nil, "WCDPWindowDefault" )
    w:Resize( 400, 110 )
	w:Show( true )
	window.W.Morale = w
	
	-- Pet Window
	w = LibGUI( "window", nil, "WCDPWindowDefault" )
    w:Resize( 400, 110 )
	w:Show( true )
	window.W.Pet = w
	
	-- This is the order the windows are displayed to the user
	table.insert( window.display, window.W.General )
	table.insert( window.display, window.W.Actions )
	table.insert( window.display, window.W.Morale )
	table.insert( window.display, window.W.Pet )
	
	--
	-- GENERAL WINDOW
	--
	-- General - Subsection Title
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General, "topleft", "topleft", 15, 10 )
    e:Font( "font_clear_medium_bold" )
    e:SetText( T["General"] )
    window.W.General.Title_Label = e
    
    -- General - Enable Sound Checkbox
	e = window.W.General( "Checkbox" )
	e:AnchorTo( window.W.General.Title_Label, "bottomleft", "topleft", 10, 5 )
	window.W.General.EnableSound_Checkbox = e
	
	-- General - Enable Sound Label
    e = window.W.Morale( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General.EnableSound_Checkbox, "right", "left", 10, 0 )
    e:Font( "font_chat_text" )
    e:SetText( T["Enable Sound With Pulse"] )
    window.W.General.EnableSound_Label = e
    
    -- General - Start Alpha Label
    e = window.W.General( "Label" )
    e:Resize( 300 )
    e:AnchorTo( window.W.General.EnableSound_Checkbox, "bottomleft", "topleft", 0, 5 )
    e:Font( "font_chat_text" )
    e:Align( "leftcenter" )
    e:SetText( T["Pulse Start Alpha (0 to 1):"] )
    window.W.General.StartAlpha_Label = e
    
    -- General - Start Alpha Textbox
    e = window.W.General( "Textbox" )
    e:Resize( 65 )
    e:Show( true )
    e:AnchorTo( window.W.General.StartAlpha_Label, "right", "left", 10, 0 )
    window.W.General.StartAlpha_Textbox = e
    
    -- General - End Alpha Label
    e = window.W.General( "Label" )
    e:Resize( 300 )
    e:AnchorTo( window.W.General.StartAlpha_Label, "bottomleft", "topleft", 0, 5 )
    e:Font( "font_chat_text" )
    e:Align( "leftcenter" )
    e:SetText( T["Pulse End Alpha (0 to 1):"] )
    window.W.General.EndAlpha_Label = e
    
    -- General - End Alpha Textbox
    e = window.W.General( "Textbox" )
    e:Resize( 65 )
    e:Show( true )
    e:AnchorTo( window.W.General.EndAlpha_Label, "right", "left", 10, 0 )
    window.W.General.EndAlpha_Textbox = e
    
    -- General - Pulse Speed Label
    e = window.W.General( "Label" )
    e:Resize( 300 )
    e:AnchorTo( window.W.General.EndAlpha_Label, "bottomleft", "topleft", 0, 5 )
    e:Font( "font_chat_text" )
    e:Align( "leftcenter" )
    e:SetText( T["Pulse Speed (sec):"] )
    window.W.General.PulseSpeed_Label = e
    
    -- General - Pulse Speed Textbox
    e = window.W.General( "Textbox" )
    e:Resize( 65 )
    e:Show( true )
    e:AnchorTo( window.W.General.PulseSpeed_Label, "right", "left", 10, 0 )
    window.W.General.PulseSpeed_Textbox = e
    
    --
   	-- ACTIONS WINDOW
   	--
    e = window.W.Actions( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.Actions, "topleft", "topleft", 15, 10 )
    e:Font( "font_clear_medium_bold" )
    e:SetText( T["Actions"] )
    window.W.Actions.Title_Label = e
    
     -- Actions - Enable Actions Checkbox
	e = window.W.Actions( "Checkbox" )
	e:AnchorTo( window.W.Actions.Title_Label, "bottomleft", "topleft", 10, 5 )
	window.W.Actions.EnableAction_Checkbox = e
	
	-- Actions - Enable Actions Label
    e = window.W.Actions( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.Actions.EnableAction_Checkbox, "right", "left", 10, 0 )
    e:Font( "font_chat_text" )
    e:SetText( T["Enable Action Pulsing"] )
    window.W.Actions.EnableAction_Label = e
    
    -- Actions - Minimum Cooldown Label
    e = window.W.Actions( "Label" )
    e:Resize( 300 )
    e:AnchorTo( window.W.Actions.EnableAction_Checkbox, "bottomleft", "topleft", 10, 10 )
    e:Font( "font_chat_text" )
    e:Align( "leftcenter" )
    e:SetText( T["Minimum Cooldown (sec):"] )
    window.W.Actions.MinimumCooldown_Label = e
    
    -- Actions - Minimum Cooldown Textbox
    e = window.W.Actions( "Textbox" )
    e:Resize( 65 )
    e:Show( true )
    e:AnchorTo( window.W.Actions.MinimumCooldown_Label, "right", "left", 10, 0 )
    window.W.Actions.MinimumCooldown_TextBox = e
    
    --
    -- MORALE WINDOW
    --
    e = window.W.Morale( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.Morale, "topleft", "topleft", 15, 10 )
    e:Font( "font_clear_medium_bold" )
    e:SetText( T["Morale"] )
    window.W.Morale.Title_Label = e
    
    -- Morale - Enable Morale Checkbox
	e = window.W.Morale( "Checkbox" )
	e:AnchorTo( window.W.Morale.Title_Label, "bottomleft", "topleft", 10, 5 )
	window.W.Morale.EnableMorale_Checkbox = e
	
	-- Morale - Enable Morale Label
    e = window.W.Morale( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.Morale.EnableMorale_Checkbox, "right", "left", 10, 0 )
    e:Font( "font_chat_text" )
    e:SetText( T["Enable Morale Pulsing"] )
    window.W.Morale.EnableMorale_Label = e
    
    -- Morale - Minimum Cooldown Label
    e = window.W.Morale( "Label" )
    e:Resize( 300 )
    e:AnchorTo( window.W.Morale.EnableMorale_Checkbox, "bottomleft", "topleft", 10, 10 )
    e:Font( "font_chat_text" )
    e:Align( "leftcenter" )
    e:SetText( T["Minimum Cooldown (sec):"] )
    window.W.Morale.MinimumCooldown_Label = e
    
    -- Morale - Minimum Cooldown Textbox
    e = window.W.Morale( "Textbox" )
    e:Resize( 65 )
    e:Show( true )
    e:AnchorTo( window.W.Morale.MinimumCooldown_Label, "right", "left", 10, 0 )
    window.W.Morale.MinimumCooldown_TextBox = e
    
    --
    -- PET WINDOW
    --
    e = window.W.Pet( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.Pet, "topleft", "topleft", 15, 10 )
    e:Font( "font_clear_medium_bold" )
    e:SetText( T["Pet Abilities"] )
    window.W.Pet.Title_Label = e
    
    -- Pet - Enable Pet Checkbox
	e = window.W.Pet( "Checkbox" )
	e:AnchorTo( window.W.Pet.Title_Label, "bottomleft", "topleft", 10, 5 )
	window.W.Pet.EnablePet_Checkbox = e
	
	-- Pet - Enable Pet Label
    e = window.W.Pet( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.Pet.EnablePet_Checkbox, "right", "left", 10, 0 )
    e:Font( "font_chat_text" )
    e:SetText( T["Enable Pet Ability Pulsing"] )
    window.W.Pet.EnablePet_Label = e
    
    -- Pet - Minimum Cooldown Label
    e = window.W.Actions( "Label" )
    e:Resize( 300 )
    e:AnchorTo( window.W.Pet.EnablePet_Checkbox, "bottomleft", "topleft", 10, 10 )
    e:Font( "font_chat_text" )
    e:Align( "leftcenter" )
    e:SetText( T["Minimum Cooldown (sec):"] )
    window.W.Pet.MinimumCooldown_Label = e
    
    -- Pet - Minimum Cooldown Textbox
    e = window.W.Pet( "Textbox" )
    e:Resize( 65 )
    e:Show( true )
    e:AnchorTo( window.W.Pet.MinimumCooldown_Label, "right", "left", 10, 0 )
    window.W.Pet.MinimumCooldown_TextBox = e
    
    initialized = true
end

function ApplyConfigSettings()
	local temp
	
	-- Start Alpha  (0 to 1)
	-- End ALpha (0 to 1)
	-- Speed	(seconds)
	db.general.sound				 	= window.W.General.EnableSound_Checkbox:GetValue()
	
	temp								= tonumber( window.W.General.StartAlpha_Textbox:GetText() ) or WCDP.DefaultSettings.alpha.start
	if( temp < 0 ) then temp = 0 end
	if( temp > 1 ) then temp = 1 end
	db.alpha.start 						= temp
	
	temp								= tonumber( window.W.General.EndAlpha_Textbox:GetText() ) or WCDP.DefaultSettings.alpha.finish
	if( temp < 0 ) then temp = 0 end
	if( temp > 1 ) then temp = 1 end
	db.alpha.finish						= temp
	
	temp								= tonumber( window.W.General.PulseSpeed_Textbox:GetText() ) or WCDP.DefaultSettings.alpha.speed
	if( temp < 0 ) then temp = 0 end
	if( temp > 2 ) then temp = 2 end
	db.alpha.speed 						= temp
	
	db.abilities.actions.enabled 		= window.W.Actions.EnableAction_Checkbox:GetValue()
	db.abilities.actions.mincooldown 	= tonumber( window.W.Actions.MinimumCooldown_TextBox:GetText() ) or WCDP.DefaultSettings.abilities.actions.mincooldown
	
	db.abilities.morale.enabled 		= window.W.Morale.EnableMorale_Checkbox:GetValue()
	db.abilities.actions.mincooldown 	= tonumber( window.W.Morale.MinimumCooldown_TextBox:GetText() ) or WCDP.DefaultSettings.abilities.morale.mincooldown
	
	db.abilities.pet.enabled 			= window.W.Pet.EnablePet_Checkbox:GetValue()
	db.abilities.actions.mincooldown 	= tonumber( window.W.Pet.MinimumCooldown_TextBox:GetText() ) or WCDP.DefaultSettings.abilities.pet.mincooldown
end

function RevertConfigSettings()
	window.W.General.EnableSound_Checkbox:SetValue( db.general.sound )
	
	window.W.General.StartAlpha_Textbox:SetText( wstring.format( L"%.1f", towstring( db.alpha.start ) ) )
	window.W.General.EndAlpha_Textbox:SetText( wstring.format( L"%.1f", towstring( db.alpha.finish ) ) )
	window.W.General.PulseSpeed_Textbox:SetText( wstring.format( L"%.1f", towstring( db.alpha.speed ) ) )

	window.W.Actions.EnableAction_Checkbox:SetValue( db.abilities.actions.enabled )
	window.W.Actions.MinimumCooldown_TextBox:SetText( wstring.format( L"%.1f", towstring( db.abilities.actions.mincooldown ) ) )
	
	window.W.Morale.EnableMorale_Checkbox:SetValue( db.abilities.morale.enabled )
	window.W.Morale.MinimumCooldown_TextBox:SetText( wstring.format( L"%.1f", towstring( db.abilities.morale.mincooldown ) ) )
	
	window.W.Pet.EnablePet_Checkbox:SetValue( db.abilities.pet.enabled )
	window.W.Pet.MinimumCooldown_TextBox:SetText( wstring.format( L"%.1f", towstring( db.abilities.pet.mincooldown ) ) )
end

window.Initialize	= InitializeWindow
window.Apply		= ApplyConfigSettings
window.Revert		= RevertConfigSettings
window.Index		= WCDPConfig.RegisterWindow( window )