<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
	<UiMod name="Wikki's Cooldown Pulse" version="2.0.4" date="2010/12" >
		<Author name="Wikki" email="wikkifizzle@gmail.com" />
		<Description text="Quick flash reminder when abilities come off of cooldown" />
		<VersionSettings gameVersion="1.4.0" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Dependencies>
			<Dependency name="EATemplate_DefaultWindowSkin" />
			<Dependency name="EASystem_LayoutEditor" />
			<Dependency name="EA_MoraleWindow" />
			<Dependency name="EA_ActionBars" />
			<Dependency name="EA_AbilitiesWindow" />
		</Dependencies>
		
		<SavedVariables>
			<SavedVariable name="WCDP.Profiles" global="true" />
		</SavedVariables>
		<Files>
			<File name="Libraries/LibStub.lua" />
			<File name="Libraries/LibGUI.lua" />
			<File name="Libraries/AceLocale-3.0.lua" />
			<File name="Libraries/LibSharedAssets.lua" />
			
			<File name="Localization/deDE.lua" />
			<File name="Localization/enUS.lua" />
			<File name="Localization/esES.lua" />
			<File name="Localization/frFR.lua" />
			<File name="Localization/itIT.lua" />
			<File name="Localization/jaJP.lua" />
			<File name="Localization/koKR.lua" />
			<File name="Localization/ruRU.lua" />
			<File name="Localization/zhCN.lua" />
			<File name="Localization/zhTW.lua" />
			
			<!-- Start Configuration Windows -->
			<File name="Configuration/WCDPConfig.xml" />
			<File name="Configuration/WCDPConfig.lua" />
			<File name="Configuration/WCDPConfig_General.lua" />
			<File name="Configuration/WCDPConfig_Filters.lua" />
			<File name="Configuration/WCDPConfig_Profiles.lua" />
			<!-- Stop Configuration Windows -->
			
			<File name="Source/Templates.xml" />
			<File name="Source/wcdp.lua" />
			
			<File name="Source/Profiles.lua" />
		</Files>
		
		<OnInitialize>
			<CreateWindow name="WCDPFrame" />
			<CreateWindow name="WCDPLayoutFrame" />
			<CreateWindow name="WCDPConfig" show="false" />
			<CallFunction name="WCDP.OnInitialize" />
		</OnInitialize>
		
		<WARInfo>
		    <Categories>
		        <Category name="ACTION_BARS" />
		        <Category name="OTHER" />
		    </Categories>
		    <Careers>
		        <Career name="BLACKGUARD" />
		        <Career name="WITCH_ELF" />
		        <Career name="DISCIPLE" />
		        <Career name="SORCERER" />
		        <Career name="IRON_BREAKER" />
		        <Career name="SLAYER" />
		        <Career name="RUNE_PRIEST" />
		        <Career name="ENGINEER" />
		        <Career name="BLACK_ORC" />
		        <Career name="CHOPPA" />
		        <Career name="SHAMAN" />
		        <Career name="SQUIG_HERDER" />
		        <Career name="WITCH_HUNTER" />
		        <Career name="KNIGHT" />
		        <Career name="BRIGHT_WIZARD" />
		        <Career name="WARRIOR_PRIEST" />
		        <Career name="CHOSEN" />
		        <Career name= "MARAUDER" />
		        <Career name="ZEALOT" />
		        <Career name="MAGUS" />
		        <Career name="SWORDMASTER" />
		        <Career name="SHADOW_WARRIOR" />
		        <Career name="WHITE_LION" />
		        <Career name="ARCHMAGE" />
		    </Careers>
		</WARInfo>

	</UiMod>
</ModuleFile>
