--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/wcdb/localization/
--

local T = LibStub("WAR-AceLocale-3.0"):NewLocale("wcdp", "frFR")
if not T then return end

-- T["(Wikki's Cooldown Pulse) use /wcdp to bring up the configuration window."] = L""
-- T["Actions"] = L""
-- T["Add Ability"] = L""
-- T["Apply"] = L""
-- T["Are you sure?"] = L""
-- T["Close"] = L""
-- T["Create"] = L""
-- T["Create New Profile:"] = L""
-- T["Delete"] = L""
-- T["Drag and drop abilities below and click 'Add Ability' to add them to the current filter."] = L""
-- T["Enable Action Pulsing"] = L""
-- T["Enable Blacklist"] = L""
-- T["Enable Filtering:"] = L""
-- T["Enable Morale Pulsing"] = L""
-- T["Enable Pet Ability Pulsing"] = L""
-- T["Enable Sound With Pulse"] = L""
-- T["Enable Whitelist"] = L""
-- T["Filter"] = L""
-- T["Filtered Abilities:"] = L""
-- T["Minimum Cooldown (sec):"] = L""
-- T["Morale"] = L""
-- T["NOTE: Profile changes take effect immediately!"] = L""
-- T["Pet Abilities"] = L""
-- T["Pulse End Alpha (0 to 1):"] = L""
-- T["Pulse Speed (sec):"] = L""
-- T["Pulse Start Alpha (0 to 1):"] = L""
-- T["Reset Profile"] = L""
-- T["Revert"] = L""
-- T["WARNING: Clicking this button will restore the current profile back to default values!"] = L""
-- T["Wikki's Cooldown Pulse %s initialized."] = L""
-- T["Wikki's Cooldown Pulse - Configuration"] = L""

