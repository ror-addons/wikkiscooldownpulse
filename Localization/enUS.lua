--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/wcdb/localization/
--

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local T = LibStub( "WAR-AceLocale-3.0" ):NewLocale( "wcdp", "enUS", true, debug )

T["(Wikki's Cooldown Pulse) use /wcdp to bring up the configuration window."] = L"(Wikki's Cooldown Pulse) use /wcdp to bring up the configuration window."
T["Actions"] = L"Actions"
T["Add Ability"] = L"Add Ability"
T["Apply"] = L"Apply"
T["Are you sure?"] = L"Are you sure?"
T["Close"] = L"Close"
T["Create"] = L"Create"
T["Create New Profile:"] = L"Create New Profile:"
T["Delete"] = L"Delete"
T["Drag and drop abilities below and click 'Add Ability' to add them to the current filter."] = L"Drag and drop abilities below and click 'Add Ability' to add them to the current filter."
T["Enable Action Pulsing"] = L"Enable Action Pulsing"
T["Enable Blacklist"] = L"Enable Blacklist"
T["Enable Filtering:"] = L"Enable Filtering:"
T["Enable Morale Pulsing"] = L"Enable Morale Pulsing"
T["Enable Pet Ability Pulsing"] = L"Enable Pet Ability Pulsing"
T["Enable Sound With Pulse"] = L"Enable Sound With Pulse"
T["Enable Whitelist"] = L"Enable Whitelist"
T["Filter"] = L"Filter"
T["Filtered Abilities:"] = L"Filtered Abilities:"
T["Minimum Cooldown (sec):"] = L"Minimum Cooldown (sec):"
T["Morale"] = L"Morale"
T["NOTE: Profile changes take effect immediately!"] = L"NOTE: Profile changes take effect immediately!"
T["Pet Abilities"] = L"Pet Abilities"
T["Pulse End Alpha (0 to 1):"] = L"Pulse End Alpha (0 to 1):"
T["Pulse Speed (sec):"] = L"Pulse Speed (sec):"
T["Pulse Start Alpha (0 to 1):"] = L"Pulse Start Alpha (0 to 1):"
T["Reset Profile"] = L"Reset Profile"
T["Revert"] = L"Revert"
T["WARNING: Clicking this button will restore the current profile back to default values!"] = L"WARNING: Clicking this button will restore the current profile back to default values!"
T["Wikki's Cooldown Pulse %s initialized."] = L"Wikki's Cooldown Pulse %s initialized."
T["Wikki's Cooldown Pulse - Configuration"] = L"Wikki's Cooldown Pulse - Configuration"
